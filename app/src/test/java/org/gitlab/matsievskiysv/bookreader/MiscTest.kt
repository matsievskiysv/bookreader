package org.gitlab.matsievskiysv.bookreader

import org.junit.Test

import org.junit.Assert.*

import org.gitlab.matsievskiysv.bookreader.helpers.Misc

class MiscTest {

    @Test
    fun testClip() {
        assertEquals(Misc.clip(5L, 1L, 10L), 5L)
        assertEquals(Misc.clip(20L, 1L, 10L), 10L)
        assertEquals(Misc.clip(0L, 1L, 10L), 1L)
    }
}
