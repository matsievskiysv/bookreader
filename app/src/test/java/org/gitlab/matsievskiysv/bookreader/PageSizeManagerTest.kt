package org.gitlab.matsievskiysv.bookreader

import org.junit.Test

import org.junit.Assert.*

import org.gitlab.matsievskiysv.bookreader.pagedview.PageSizeManager

class PageSizeManagerTest {

    private val manager = PageSizeManager(5) {
        viewSize, pageSize -> viewSize.x
    }

    @Test
    fun testCache() {
        runBlocking {
	        async { cache.clear() }.await()
            // push
            async { cache.push(10, "a") }.await()
	        async { cache.push(20, "b") }.await()
	        async { cache.push(30, "c") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("a", "b", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(2, 1, 0))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(30, 20, 10))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("c", "b", "a"))
	        async { cache.push(40, "d") }.await()
	        async { cache.push(50, "e") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("a", "b", "c", "d", "e"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(4, 3, 2, 1, 0))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(50, 40, 30, 20, 10))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("e", "d", "c", "b", "a"))
	        async { cache.push(60, "f") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("f", "b", "c", "d", "e"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(0, 4, 3, 2, 1))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(60, 50, 40, 30, 20))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("f", "e", "d", "c", "b"))
	        async { cache.push(30, "g") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("f", "b", "g", "d", "e"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(2, 0, 4, 3, 1))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(30, 60, 50, 40, 20))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("g", "f", "e", "d", "b"))
	        async { cache.push(40, "d") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("f", "b", "g", "d", "e"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(3, 2, 0, 4, 1))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(40, 30, 60, 50, 20))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("d", "g", "f", "e", "b"))
            // remove
            assertEquals(async { cache.pop(60) }.await(),
                         "f")
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "g", "d", "e"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(2, 1, 3, 0))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(40, 30, 50, 20))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("d", "g", "e", "b"))
            assertEquals(async { cache.pop(30) }.await(),
                         "g")
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "d", "e"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(1, 2, 0))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(40, 50, 20))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("d", "e", "b"))
            async { cache.push(10, "a") }.await()
	        async { cache.push(20, "b") }.await()
	        async { cache.push(30, "c") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "d", "e", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(4, 0, 3, 1, 2))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(30, 20, 10, 40, 50))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("c", "b", "a", "d", "e"))
	        async { cache.push(60, "f") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "d", "f", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(2, 4, 0, 3, 1))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(60, 30, 20, 10, 40))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("f", "c", "b", "a", "d"))
            assertEquals(async { cache.get(10) }.await(),
                         "a")
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "d", "f", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(3, 2, 4, 0, 1))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(10, 60, 30, 20, 40))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("a", "f", "c", "b", "d"))
            assertEquals(async { cache.get(30) }.await(),
                         "c")
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "d", "f", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(4, 3, 2, 0, 1))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(30, 10, 60, 20, 40))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("c", "a", "f", "b", "d"))
            // capacity
	        async { cache.setCapacity(3) }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("f", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(2, 1, 0))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(30, 10, 60))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("c", "a", "f"))
            assertEquals(async { cache.get(10) }.await(),
                         "a")
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("f", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(1, 2, 0))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(10, 30, 60))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("a", "c", "f"))
	        async { cache.push(20, "b") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "a", "c"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(0, 1, 2))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(20, 10, 30))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("b", "a", "c"))
	        async { cache.setCapacity(6) }.await()
	        async { cache.push(1, "x") }.await()
	        async { cache.push(2, "y") }.await()
	        async { cache.push(3, "z") }.await()
            assertEquals(async { cache.getObjects() }.await(),
                         listOf("b", "a", "c", "x", "y", "z"))
            assertEquals(async { cache.getOrder() }.await(),
                         listOf(5, 4, 3, 0, 1, 2))
            assertEquals(async { cache.getLabels() }.await(),
                         listOf(3, 2, 1, 20, 10, 30))
            assertEquals(async { cache.getObjectsOrdered() }.await(),
                         listOf("z", "y", "x", "b", "a", "c"))
        }
        // async calls
        runBlocking {
	        async { cache.clear() }.await()
            var c1 = async { cache.push(1, "a") }
            var c2 = async { cache.push(2, "b") }
            var c3 = async { cache.push(3, "c") }
            awaitAll(c1, c2, c3)
            assertEquals(async { cache.getObjectsOrdered() }.await().sortedBy { it },
                         listOf("a", "b", "c"))
            c1 = async { cache.pop(1) }
            c2 = async { cache.pop(2) }
            c3 = async { cache.push(4, "d") }
            awaitAll(c1, c2, c3)
            assertEquals(async { cache.getObjectsOrdered() }.await().sortedBy { it },
                         listOf("c", "d"))
        }
    }
}
