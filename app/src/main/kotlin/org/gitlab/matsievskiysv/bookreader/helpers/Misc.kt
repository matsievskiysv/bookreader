package org.gitlab.matsievskiysv.bookreader.helpers

import kotlin.math.*

fun <T : Comparable<T>> clip(v: T, start: T, end: T): T {
    if (v < start) {
        return start
    } else {
        if (v > end) {
            return end
        } else {
            return v
        }
    }
}
