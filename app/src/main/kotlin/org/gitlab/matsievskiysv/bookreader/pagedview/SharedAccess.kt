package org.gitlab.matsievskiysv.bookreader.pagedview

import kotlinx.coroutines.runBlocking

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

public class SharedAccess<T> (asset: T) {
    private val asset = asset
    private val mutex = Mutex()

    suspend fun <R> withLock(func: (T) -> R): R {
        return mutex.withLock {
            asset.let(func)
        }
    }

    fun <R> withBlock(func: (T) -> R): R {
        return runBlocking {
            withLock(func)
        }
    }
}
