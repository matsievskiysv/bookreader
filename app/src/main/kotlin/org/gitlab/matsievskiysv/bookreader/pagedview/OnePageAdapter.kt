// package org.gitlab.matsievskiysv.bookreader.pagedview

// import android.graphics.Bitmap
// import android.graphics.Canvas
// import android.graphics.Point
// import android.graphics.PointF
// import android.graphics.Rect
// import android.graphics.RectF
// import android.graphics.Matrix
// import android.graphics.Paint

// import android.view.ScaleGestureDetector
// import android.animation.ObjectAnimator
// import android.view.animation.LinearInterpolator
// import android.content.Context
// import android.view.View


// import kotlin.math.*

// import kotlinx.coroutines.async
// import kotlinx.coroutines.runBlocking

// import org.gitlab.matsievskiysv.bookreader.helpers.Logger

// @kotlin.ExperimentalStdlibApi
// class OnePageAdapter constructor(context: Context,
//                                  view: View,
//                                  document: SharedAccess<IPagedDocument>):
//     PagedAdapter(context, view, document) {

//     private val logger = Logger(context, this::class.simpleName!!)

//     override fun pageToViewScale(page: Int): Float {
//         return when (fillMode) {
//             PagedAdapter.Companion.FillMode.WIDTH -> {
//                 pageBounds[page].pageSize.x *
//                 pageBounds[page].cropBounds.width() / viewSize.x
//             }
//             PagedAdapter.Companion.FillMode.HEIGHT -> {
//                 pageBounds[page].pageSize.y *
//                 pageBounds[page].cropBounds.height() / viewSize.y
//             }
//             PagedAdapter.Companion.FillMode.PAGE -> {
//                 val scaleX = pageBounds[page].pageSize.x *
//                 pageBounds[page].cropBounds.width() / viewSize.x
//                 val scaleY = pageBounds[page].pageSize.y *
//                 pageBounds[page].cropBounds.height() / viewSize.y
//                 if (abs(scaleX-1) < abs(scaleY-1)) scaleX else scaleY
//             }
//         }
//     }

//     override fun updateRequiredTiles(size: Point) {
//         lateinit var pointStart: Point
//         lateinit var pointEnd: Point

//         val (pageSize, tileSize) = getPageTileSize(currentPage)

//         if (size.y > 0f) {
//             // move forward
//             pointStart = (curPos * pageSize).floor()
//             pointEnd = min(pointStart + size, pageSize)
//         } else {
//             // move backward
//             pointEnd = (curPos * pageSize).ceil()
//             pointStart = max(pointStart + size, Point(0, 0))
//         }
//         requiredTiles.clear()
//         requiredTiles += getTiles(currentPage, tileSize,
//                                   Rect(pointStart.x, pointStart.y,
//                                        pointEnd.x, pointEnd.y))
//     }

//     private fun getTiles(page: Int, tileSize: Point, view: Rect): MutableList<TileIndex> {
//         val leftIndex = floor(view.left.toFloat() / tileSize.x).toInt()
//         val topIndex = floor(view.top.toFloat() / tileSize.y).toInt()
//         val rightIndex = ceil(view.right.toFloat() / tileSize.x).toInt()
//         val bottomIndex = ceil(view.bottom.toFloat() / tileSize.y).toInt()

//         val tileList = ArrayList<TileIndex>((rightIndex-leftIndex) * (bottomIndex-topIndex))

//         for (i in leftIndex..(rightIndex-1)) {
//             for (j in topIndex..(bottomIndex-1)) {
//                 tileList.add(TileIndex(page, Point(i, j), tiles))
//             }
//         }
//         return tileList
//     }

//     override fun offsetPage(page: Int): PointF {
//         val (pageSize, _) = getPageTileSize(page)
//         return when (fillMode) {
//             PagedAdapter.Companion.FillMode.WIDTH ->
//                 PointF(0f, if (viewSize.y > pageSize.y) (viewSize.y - pageSize.y).toFloat() / 2 else 0f)
//             PagedAdapter.Companion.FillMode.HEIGHT ->
//                 PointF(if (viewSize.x > pageSize.x) (viewSize.x - pageSize.x).toFloat() / 2 else 0f, 0f)
//             PagedAdapter.Companion.FillMode.PAGE -> if ((viewSize.x - pageSize.x) >=
//                                                              (viewSize.y - pageSize.y)) {
//                 PointF(if (viewSize.x > pageSize.x) (viewSize.x - pageSize.x).toFloat() / 2 else 0f, 0f)
//             } else {
//                 PointF(0f, if (viewSize.y > pageSize.y) (viewSize.y - pageSize.y).toFloat() / 2 else 0f)
//             }
//         }
//     }

//     override fun composeTiles(canvas: Canvas) {
//         // ObjectAnimator.ofFloat(view, "alpha", 0f).apply {
//         //     duration = 20
//         //     start()
//         // }
//         logger.v("drawing ${requiredTiles.size} tiles")
//         for (t in requiredTiles) {
//             val (_, tileSize) = getPageTileBitmapSize(currentPage)
//             val (pageSize, _) = getPageTileSize(currentPage)
//             val tileSizeF = PointF(tileSize)
//             val pos = pageSize * curPos
//             val offset = offsetPage(t.page)

//             bitmapCache.getSync(t)?.withBlock {
//                 val matrix = Matrix().apply{
//                     postTranslate(tileSizeF.x * t.tile.x,
//                                   tileSizeF.y * t.tile.y)
//                     postScale(zoomLevel / tiles.x / bitmapScaleFactor,
//                               zoomLevel / tiles.y / bitmapScaleFactor)
//                     postTranslate(offset.x, offset.y)
//                     postTranslate(-pos.x, -pos.y)
//                 }
//                 canvas.drawBitmap(it, matrix, null)
//             } ?: view.postInvalidate()
//         }
//         // ObjectAnimator.ofFloat(view, "alpha", 1f).apply {
//         //     setStartDelay(20)
//         //     duration = 20
//         //     start()
//         // }
//     }

//     override fun fling(velocityX: Float, velocityY: Float): Boolean {
//         if (abs(velocityX) > abs(2 * velocityY) && abs(velocityX) > 5000 && zoomLevel == 1f) {
//             if (velocityX < 0) {
//                 currentPage++
//                 curPos = PointF(0f, 0f)
//                 view.postInvalidate()
//             } else if (velocityX > 0) {
//                 currentPage--
//                 curPos = PointF(0f, 0f)
//                 view.postInvalidate()
//             }
//             return true
//         }
//         return false
//     }

//     override fun scroll(distanceX: Float, distanceY: Float): Boolean {
//         val (pageSize, _) = getPageTileSize(currentPage)
//         val pos = curPos * pageSize + PointF(distanceX, distanceY)
//         val newPos = pos.clip(PointF(0f, 0f), PointF(pageSize - viewSize)) / pageSize
//         if (curPos != newPos) {
//             curPos = newPos
//             view.postInvalidate()
//             return true
//         }
//         return false
//     }

//     override fun zoom(detector: ScaleGestureDetector): Boolean {
//         val (pageSize, _) = getPageTileSize(currentPage)
//         // translate to the focus point with vector length proportional to zoom level
//         val focus = curPos * pageSize + PointF(detector.getFocusX(), detector.getFocusY())
//         // val viewCenter = PointF(curPos.x * pageSize.x + viewSize.x.toFloat() / 2,
//         //                         curPos.y * pageSize.y + viewSize.y.toFloat() / 2)
//         val translateVector = (focus - curPos * pageSize) * (detector.getScaleFactor() - 1)
//         val pos = curPos * pageSize + translateVector
//         val newPos = pos.clip(PointF(0f, 0f), PointF(pageSize - viewSize)) / pageSize
//         val zoom = clip(zoomLevel * detector.getScaleFactor(), 1f, 100f)
//         if (zoom != zoomLevel || newPos != curPos) {
//             zoomLevel = zoom
//             curPos = newPos
//             view.postInvalidate()
//             return true
//         }
//         return false
//     }

// }
