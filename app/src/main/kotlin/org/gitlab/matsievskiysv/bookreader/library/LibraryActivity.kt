package org.gitlab.matsievskiysv.bookreader.library

import org.gitlab.matsievskiysv.bookreader.R

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.content.Context
import android.app.Activity;
// import android.view.View
// import android.widget.Switch
import android.widget.Toast
import android.content.Intent
import android.widget.Button
import android.net.Uri
import android.view.Menu
import android.view.MenuItem
import android.view.MenuInflater


import androidx.activity.result.contract.ActivityResultContracts.OpenDocument
// import androidx.fragment.app.Fragment
// import androidx.fragment.app.FragmentActivity
// import androidx.viewpager2.adapter.FragmentStateAdapter
// import androidx.viewpager2.widget.ViewPager2
// import androidx.recyclerview.widget.RecyclerView
// import androidx.recyclerview.widget.LinearLayoutManager

import org.gitlab.matsievskiysv.bookreader.helpers.Logger
import org.gitlab.matsievskiysv.bookreader.viewer.ViewerActivity

@kotlin.ExperimentalStdlibApi
class LibraryActivity: AppCompatActivity() {
    private lateinit var logger: Logger
    private lateinit var toast: Toast

    val getFile = registerForActivityResult(OpenDocument()) {
        uri: Uri? ->
            uri?.let{
                val intent = Intent(this, ViewerActivity::class.java).apply {
                    setData(uri)
                }
                startActivity(intent)
            } ?: toast.apply{
                setText("cannot open file")
                show()
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // val sharedPref = getSharedPreferences("org.gitlab.matsievskiysv.bookreader.MAIN",
        //                                       Context.MODE_PRIVATE)
        // with (sharedPref.edit()) {
        //     putInt("LOGLEVEL", Logger.VERBOSE)
        //     commit()
        // }
        logger = Logger(this, "LibraryActivity")
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT)
        setContentView(R.layout.library)
        setSupportActionBar(findViewById(R.id.library_toolbar))
        // val openFile = findViewById<Button>(R.id.library_toolbar_open_file)
        // openFile.setOnClickListener{ getFile.launch(arrayOf("application/pdf")) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        getMenuInflater().inflate(R.menu.library_menu, menu)
        return true
    }

     // actions on click menu items
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.library_toolbar_open_file -> {
            getFile.launch(arrayOf("application/pdf"))
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

}
