package org.gitlab.matsievskiysv.bookreader.pagedview

import android.graphics.Point
import android.graphics.PointF
import android.graphics.Rect
import android.graphics.RectF

import kotlin.math.*

fun <T : Comparable<T>> clip(v: T, start: T, end: T): T {
    if (start >= end) {
        return start
    }
    if (v < start) {
        return start
    } else {
        if (v > end) {
            return end
        } else {
            return v
        }
    }
}

// move rectangle so that it is inside
fun adjustRect(rect: RectF, target: RectF): RectF {
    if (rect.width() >= target.width()) {
        rect.left = target.left
        rect.right = target.right
    } else {
        // adjust X
        if (target.left - rect.left > 0) {
            rect.offsetTo(target.left, rect.top)
        } else if (rect.right - target.right > 0) {
            rect.offsetTo(target.right - rect.width(), rect.top)
        }
    }
    if (rect.height() >= target.height()) {
        rect.top = target.top
        rect.bottom = target.bottom
    } else {
        // adjust Y
        if (target.top - rect.top > 0) {
            rect.offsetTo(rect.left, target.top)
        } else if (rect.bottom - target.bottom > 0) {
            rect.offsetTo(rect.left, target.bottom - rect.height())
        }
    }
    return rect
}

// construct Rect form two points

fun Rect(p1: Point, p2: Point): Rect {
    return Rect(p1.x, p1.y, p2.x, p2.y)
}

fun RectF(p1: PointF, p2: PointF): RectF {
    return RectF(p1.x, p1.y, p2.x, p2.y)
}

// destructing declaration

operator fun Point.component1(): Int {
    return this.x
}

operator fun Point.component2(): Int {
    return this.y
}

operator fun PointF.component1(): Float {
    return this.x
}

operator fun PointF.component2(): Float {
    return this.y
}

operator fun Rect.component1(): Int {
    return this.left
}

operator fun Rect.component2(): Int {
    return this.top
}

operator fun Rect.component3(): Int {
    return this.right
}

operator fun Rect.component4(): Int {
    return this.bottom
}

operator fun RectF.component1(): Float {
    return this.left
}

operator fun RectF.component2(): Float {
    return this.top
}

operator fun RectF.component3(): Float {
    return this.right
}

operator fun RectF.component4(): Float {
    return this.bottom
}

fun Rect.topLeft(): Point {
    return Point(this.left, this.top)
}

fun Rect.topRight(): Point {
    return Point(this.right, this.top)
}

fun Rect.bottomRight(): Point {
    return Point(this.right, this.bottom)
}

fun Rect.bottomLeft(): Point {
    return Point(this.left, this.bottom)
}

fun RectF.topLeft(): PointF {
    return PointF(this.left, this.top)
}

fun RectF.topRight(): PointF {
    return PointF(this.right, this.top)
}

fun RectF.bottomRight(): PointF {
    return PointF(this.right, this.bottom)
}

fun RectF.bottomLeft(): PointF {
    return PointF(this.left, this.bottom)
}

// round

fun PointF.floor(): Point {
    return Point(floor(this.x).toInt(),
                 floor(this.y).toInt())
}

fun PointF.ceil(): Point {
    return Point(ceil(this.x).toInt(),
                 ceil(this.y).toInt())
}

fun PointF.round(): Point {
    return Point(round(this.x).toInt(),
                 round(this.y).toInt())
}

fun Point.clip(start: Point, end: Point): Point {
    return Point(clip(this.x, start.x, end.x),
                 clip(this.y, start.y, end.y))
}

fun PointF.clip(start: PointF, end: PointF): PointF {
    return PointF(clip(this.x, start.x, end.x),
                  clip(this.y, start.y, end.y))
}

fun RectF.roundOut(): Rect {
    return Rect(floor(this.left).toInt(), floor(this.top).toInt(),
                ceil(this.right).toInt(), ceil(this.bottom).toInt())
}

// plus

operator fun Point.plus(other: Int): Point {
    return Point(this.x.plus(other), this.y.plus(other))
}

operator fun Point.plus(other: Float): PointF {
    return PointF(this.x.plus(other), this.y.plus(other))
}

operator fun PointF.plus(other: Int): PointF {
    return PointF(this.x.plus(other), this.y.plus(other))
}

operator fun PointF.plus(other: Float): PointF {
    return PointF(this.x.plus(other), this.y.plus(other))
}


operator fun Point.plus(other: Point): Point {
    return Point(this.x.plus(other.x), this.y.plus(other.y))
}

operator fun Point.plus(other: PointF): PointF {
    return PointF(this.x.plus(other.x), this.y.plus(other.y))
}

operator fun PointF.plus(other: Point): PointF {
    return PointF(this.x.plus(other.x), this.y.plus(other.y))
}

operator fun PointF.plus(other: PointF): PointF {
    return PointF(this.x.plus(other.x), this.y.plus(other.y))
}

// minus

operator fun Point.minus(other: Int): Point {
    return Point(this.x.minus(other), this.y.minus(other))
}

operator fun Point.minus(other: Float): PointF {
    return PointF(this.x.minus(other), this.y.minus(other))
}

operator fun PointF.minus(other: Int): PointF {
    return PointF(this.x.minus(other), this.y.minus(other))
}

operator fun PointF.minus(other: Float): PointF {
    return PointF(this.x.minus(other), this.y.minus(other))
}


operator fun Point.minus(other: Point): Point {
    return Point(this.x.minus(other.x), this.y.minus(other.y))
}

operator fun Point.minus(other: PointF): PointF {
    return PointF(this.x.minus(other.x), this.y.minus(other.y))
}

operator fun PointF.minus(other: Point): PointF {
    return PointF(this.x.minus(other.x), this.y.minus(other.y))
}

operator fun PointF.minus(other: PointF): PointF {
    return PointF(this.x.minus(other.x), this.y.minus(other.y))
}

// times

operator fun Point.times(other: Int): Point {
    return Point(this.x.times(other), this.y.times(other))
}

operator fun Point.times(other: Float): PointF {
    return PointF(this.x.times(other), this.y.times(other))
}

operator fun PointF.times(other: Int): PointF {
    return PointF(this.x.times(other), this.y.times(other))
}

operator fun PointF.times(other: Float): PointF {
    return PointF(this.x.times(other), this.y.times(other))
}


operator fun Point.times(other: Point): Point {
    return Point(this.x.times(other.x), this.y.times(other.y))
}

operator fun Point.times(other: PointF): PointF {
    return PointF(this.x.times(other.x), this.y.times(other.y))
}

operator fun PointF.times(other: Point): PointF {
    return PointF(this.x.times(other.x), this.y.times(other.y))
}

operator fun PointF.times(other: PointF): PointF {
    return PointF(this.x.times(other.x), this.y.times(other.y))
}

// div

operator fun Point.div(other: Int): Point {
    return Point(this.x.div(other), this.y.div(other))
}

operator fun Point.div(other: Float): PointF {
    return PointF(this.x.div(other), this.y.div(other))
}

operator fun PointF.div(other: Int): PointF {
    return PointF(this.x.div(other), this.y.div(other))
}

operator fun PointF.div(other: Float): PointF {
    return PointF(this.x.div(other), this.y.div(other))
}


operator fun Point.div(other: Point): Point {
    return Point(this.x.div(other.x), this.y.div(other.y))
}

operator fun Point.div(other: PointF): PointF {
    return PointF(this.x.div(other.x), this.y.div(other.y))
}

operator fun PointF.div(other: Point): PointF {
    return PointF(this.x.div(other.x), this.y.div(other.y))
}

operator fun PointF.div(other: PointF): PointF {
    return PointF(this.x.div(other.x), this.y.div(other.y))
}

// min

fun min(a: Point, b: Point): Point {
    return Point(min(a.x, b.x), min(a.y, b.y))
}

fun min(a: PointF, b: PointF): PointF {
    return PointF(min(a.x, b.x), min(a.y, b.y))
}

// min

fun max(a: Point, b: Point): Point {
    return Point(max(a.x, b.x), max(a.y, b.y))
}

fun max(a: PointF, b: PointF): PointF {
    return PointF(max(a.x, b.x), max(a.y, b.y))
}

// concat

fun Point.catH(other: Point): Point {
    return Point(this.x + other.x, max(this.y, other.y))
}

fun Point.catH(other: PointF): PointF {
    return PointF(this.x.toFloat() + other.x, max(this.y.toFloat(), other.y))
}

fun PointF.catH(other: Point): PointF {
    return PointF(this.x + other.x.toFloat(), max(this.y, other.y.toFloat()))
}

fun PointF.catH(other: PointF): PointF {
    return PointF(this.x + other.x, max(this.y, other.y))
}


fun Point.catV(other: Point): Point {
    return Point(max(this.y, other.y), this.x + other.x)
}

fun Point.catV(other: PointF): PointF {
    return PointF(max(this.y.toFloat(), other.y), this.x.toFloat() + other.x)
}

fun PointF.catV(other: Point): PointF {
    return PointF(max(this.y, other.y.toFloat()), this.x + other.x.toFloat())
}

fun PointF.catV(other: PointF): PointF {
    return PointF(max(this.y, other.y), this.x + other.x)
}

// map

fun Point.map(func: (Int) -> Int): Point {
    return Point(func(x), func(y))
}

fun Point.map(func: (Int) -> Float): PointF {
    return PointF(func(x), func(y))
}

fun PointF.map(func: (Float) -> Float): PointF {
    return PointF(func(x), func(y))
}

fun PointF.map(func: (Float) -> Int): Point {
    return Point(func(x), func(y))
}

fun Rect.map(func: (Int) -> Int): Rect {
    return Rect(func(left), func(top), func(right), func(bottom))
}

fun Rect.map(func: (Int) -> Float): RectF {
    return RectF(func(left), func(top), func(right), func(bottom))
}

fun RectF.map(func: (Float) -> Float): RectF {
    return RectF(func(left), func(top), func(right), func(bottom))
}

fun RectF.map(func: (Float) -> Int): Rect {
    return Rect(func(left), func(top), func(right), func(bottom))
}
