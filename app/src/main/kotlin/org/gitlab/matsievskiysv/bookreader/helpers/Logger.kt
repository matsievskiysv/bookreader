package org.gitlab.matsievskiysv.bookreader.helpers

import org.gitlab.matsievskiysv.bookreader.R

import android.content.SharedPreferences
import android.util.Log
import android.content.pm.PackageInfo
import android.content.Context

class Logger constructor(private val context: Context, private val prefix: String = "", level: Int? = null) {

    companion object {
        const val VERBOSE = 0
        const val DEBUG = 1
        const val INFO = 2
        const val WARN = 3
        const val ERROR = 4
        const val FATAL = 5
    }

    private val level: Int = level ?: context
        .getSharedPreferences("MAIN", Context.MODE_PRIVATE).getInt("LOGLEVEL", ERROR)
    private val tag: String = context.getPackageName()

    init {
        Log.i("BookReader", "application tag: ${tag}, loglevel: ${level}")
    }

    // verbose
    fun v(msg: String) {
	if (level <= VERBOSE) {
	    android.util.Log.v(tag, prefix + ":\t" + msg)
	}
    }

    // debug
    fun d(msg: String) {
	if (level <= DEBUG) {
	    android.util.Log.d(tag, prefix + ":\t" + msg)
	}
    }

    // info
    fun i(msg: String) {
	if (level <= INFO) {
	    android.util.Log.i(tag, prefix + ":\t" + msg)
	}
    }

    // warn
    fun w(msg: String) {
	if (level <= WARN) {
	    android.util.Log.w(tag, prefix + ":\t" + msg)
	}
    }

    // error
    fun e(msg: String) {
	if (level <= ERROR) {
	    android.util.Log.e(tag, prefix + ":\t" + msg)
	}
    }

    // fatal
    fun f(msg: String) {
	if (level <= FATAL) {
	    android.util.Log.wtf(tag, prefix + ":\t" + msg)
	}
    }
}
