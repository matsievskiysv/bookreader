package org.gitlab.matsievskiysv.bookreader.pagedview

import android.graphics.Canvas
import android.graphics.Point

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.async

@kotlin.ExperimentalStdlibApi
class PageCache constructor(document: SharedAccess<IPagedDocument>, capacity: Int) {

    private val doc = document
    private val cache =
        Cache<Int, IPagedPage>(
            capacity,
            null,
            { _, page -> page.close() }
        )

    constructor(document: SharedAccess<IPagedDocument>): this(document, 20)

    suspend fun withPage(pageNum: Int, func: (IPagedPage) -> Unit) {
        doc.withLock {
            val page = cache.getSync(pageNum) ?:
                it.page(pageNum)?.also{ cache.pushSync(pageNum, it) }
            if (page != null) {
                func(page)
            }
        }
    }

    suspend fun setCapacity(capacity: Int) = cache.setCapacity(capacity)

    suspend fun getCapacity(): Int = cache.getCapacity()

    suspend fun ensureCapacity(newc: Int) = cache.ensureCapacity(newc)

    suspend fun clear() = cache.clear()
}
