package org.gitlab.matsievskiysv.bookreader.pagedview

import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.RectF

interface IPagedPage {
    // close page
    fun close() {}
    // get page size
    fun size(): Point { return Point(0, 0) }
    // draw page
    fun draw(bitmap: Bitmap, size: Point, crop: RectF)
}
