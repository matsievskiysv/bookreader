package org.gitlab.matsievskiysv.bookreader.pagedview

import android.graphics.Point
import android.graphics.PointF
import android.graphics.Paint
import android.graphics.Color
import android.graphics.Canvas
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.Rect
import android.graphics.RectF

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.MotionEvent
import android.content.res.Configuration
import android.view.GestureDetector
import android.view.ScaleGestureDetector
import android.app.Activity
import android.animation.ObjectAnimator

import android.widget.Toast

import kotlin.math.*

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

import org.gitlab.matsievskiysv.bookreader.helpers.clip
import org.gitlab.matsievskiysv.bookreader.helpers.Logger

@kotlin.ExperimentalStdlibApi
class PagedView constructor(context: Context,
                            attrs: AttributeSet?) : View(context, attrs) {
    // there are two major coordinate systems: document system and view system
    // document system uses original page sizes
    // view system represents screen view port
    // additional page coordinate system is just a document system translated to the page origin

    // document is split in pages
    // each page may be split in tiles for partial rendering
    // number of tiles is calculated dynamically

    // logger
    private val logger = Logger(context, this::class.simpleName!!)

    // show emergency message and setup fallback view
    private fun panic(msg: String) {
        logger.e(msg)
        throw RuntimeException(msg)                                                        // TODO replace with fallback
    }

    // private lateinit var document: SharedAccess<IPagedDocument> // document handler

    // private lateinit var adapter: PagedAdapter                                                           // view adapter

    // private var adapterClass = ::VerticalScrollAdapter                                                     // view adapter

    // check if document is initialized
    // private fun checkDocument() {
    //     if (!this::document.isInitialized) {
    //         panic("Document is not initialized")
    //     }
    // }

    // set document
    fun setDocument(doc: SharedAccess<IPagedDocument>) {
    //     document = doc
    //     adapter = adapterClass(getContext(), this, doc)
    }

    // save view state
    fun saveState() {
        // TODO fill
    }

    // load saved state
    fun loadSavedState() {
        // TODO fill
    }

    private var viewSize = Point(0, 0)

    // size change callback
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        logger.v("onSizeChanged")
	    viewSize = Point(w, h)
    }

    // redraw callback
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // adapter.draw(canvas, viewSize)
    }

    // override fun onConfigurationChanged(newConfig: Configuration) {
    //     logger.v("configuration change")
    //     postInvalidate()
    // }

    private val scaleListener = object: ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            // return adapter.zoom(detector)
            return true
        }
    }

    private val scaleDetector: ScaleGestureDetector = ScaleGestureDetector(context, scaleListener)

    private val simpleListener = object: GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onScroll(e1: MotionEvent,
                              e2: MotionEvent,
                              distanceX: Float,
                              distanceY: Float): Boolean {
            // return adapter.scroll(distanceX, distanceY)
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            // return adapter.fling(velocityX, velocityY)
            return true
        }

        // override fun onDoubleTap(e: MotionEvent): Boolean {
        //     logger.v("gesture double tap")
        //     // immersive = !immersive
        //     // when (immersive) {
        //     //     true -> hideSystemUI()
        //     //     false -> showSystemUI()
        //     // }
        //     return true;
        // }
    }

    private val simpleDetector: GestureDetector = GestureDetector(context, simpleListener)

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return scaleDetector.onTouchEvent(event).let {
            scaleDetector.isInProgress()
        } || simpleDetector.onTouchEvent(event).let {
            it
                // if (!result) {
                //     if (event.action == MotionEvent.ACTION_UP) {
                //         logger.v("gesture action up")
                //         // stopScrolling()
                //         true
                //     } else false
                // } else true
        }
    }

    // override fun onWindowFocusChanged(hasFocus: Boolean) {
    //     super.onWindowFocusChanged(hasFocus)
    //     if (hasFocus) {
    //         when (immersive) {
    //             true -> hideSystemUI()
    //             false -> showSystemUI()
    //         }
    //     }
    // }

    // private fun hideSystemUI() {
    //     // Enables regular immersive mode.
    //     // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
    //     // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    //     window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
    //                                            // Set the content to appear under the system bars so that the
    //                                            // content doesn't resize when the system bars hide and show.
    //                                            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    //                                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
    //                                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    //                                            // Hide the nav bar and status bar
    //                                            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    //                                            or View.SYSTEM_UI_FLAG_FULLSCREEN)
    // }

    // // Shows the system bars by removing all the flags
    // // except for the ones that make the content appear under the system bars.
    // private fun showSystemUI() {
    //     window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    //                                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
    //                                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    // }

}
