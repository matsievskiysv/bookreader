package org.gitlab.matsievskiysv.bookreader.settings

import org.gitlab.matsievskiysv.bookreader.R

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


// import android.content.Context;
// import android.view.View;
// import android.widget.Switch;
// import android.widget.Toast;

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.fragment.app.FragmentActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class GlobalSettingsActivity: AppCompatActivity() {
    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager2? = null

    private val tabNames = arrayOf("UI",
                                   "View")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.global_settings)
        // https://inducesmile.com/kotlin-source-code/how-to-use-tablayout-with-viewpager2-in-kotlin/
        viewPager = findViewById(R.id.global_settings_pager) as ViewPager2
        tabLayout = findViewById(R.id.global_settings_tabs) as TabLayout
        viewPager!!.setAdapter(ScreenSlidePagerAdapter(supportFragmentManager, lifecycle))
        TabLayoutMediator(tabLayout!!, viewPager!!,
                          TabLayoutMediator.TabConfigurationStrategy{tab, position -> tab.text = tabNames[position]}
        ).attach()
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager?, lifecycle: Lifecycle): FragmentStateAdapter(fm!!, lifecycle) {

        private val fragments = arrayOf(::GlobalSettingsUiFragment,
                                        ::GlobalSettingsViewFragment)

        override fun createFragment(position: Int): Fragment {
            return fragments[position]()
        }

        override fun getItemCount(): Int {
            return fragments.size
        }
                    // class ScreenSlidePagerAdapter(val fa: FragmentActivity): FragmentStateAdapter() {

                        // interface SettingFragmentConstructor {
                            //     Fragment create();
                            // }

                            // // Array of PageViewer2 pages
                            // private SettingFragmentConstructor[] fragments =
                                //     new SettingFragmentConstructor[] {
                                    //     new SettingFragmentConstructor() {
                                        //         public Fragment create() {return GlobalSettingsUiFragment.newInstance("", "");}
                                        //     },
                                        //     new SettingFragmentConstructor() {
                                            //         public Fragment create() {return GlobalSettingsViewFragment.newInstance("", "");
                                            //         }},
                                            // };

                                            // constructor(fa: FragmentActivity): super(fa) { }

                                            //     override fun createFragment(val position: Int): Fragment {
                                                //         // return fragments[position].create();
                                                //         return GlobalSettingsUiFragment
                                                //     }

                                                //     override fun getItemCount(): Int {
                                                    //         return 1
                                                    //     }
                }
}
