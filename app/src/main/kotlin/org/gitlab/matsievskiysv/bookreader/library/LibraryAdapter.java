package org.gitlab.matsievskiysv.bookreader.library;

import org.gitlab.matsievskiysv.bookreader.R;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.LayoutInflater;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.MyViewHolder> {
    private String[] names;
    private Integer[] pages;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView page;

        public MyViewHolder(View v, TextView name, TextView page) {
            super(v);
            this.name = name;
            this.page = page;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public LibraryAdapter(String[] names, Integer[] pages) {
        this.names = names;
        this.pages = pages;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View view = (View) LayoutInflater.from(parent.getContext())
            .inflate(R.layout.book_text, parent, false);
        TextView name = view.findViewById(R.id.book_text_name);
        TextView page = view.findViewById(R.id.book_text_page);
        MyViewHolder vh = new MyViewHolder(view, name, page);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.name.setText(names[position]);
        holder.page.setText(pages[position].toString());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return names.length;
    }
}

