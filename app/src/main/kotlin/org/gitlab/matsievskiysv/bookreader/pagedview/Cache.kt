package org.gitlab.matsievskiysv.bookreader.pagedview

import kotlin.collections.ArrayDeque

import kotlinx.coroutines.runBlocking

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

@kotlin.ExperimentalStdlibApi
class Cache<N, T> constructor(capacity: Int,
							  initializer: ((N, T) -> Unit)? = null,
							  destructor: ((N, T) -> Unit)? = null) {
    private val mutex = Mutex()

    init{
        if (capacity < 1) {
            throw(IllegalArgumentException("Capacity must be positive"))
        }
    }

    internal data class QueueItem<N> constructor(var index: Int, val label: N)

    // capacity of cache
    private var capacity = capacity
    // array of objects T
    private val objects = ArrayList<T>(capacity)
    // queue of object indexes and labels N sorted from least to most recently used
    private val queue = ArrayDeque<QueueItem<N>>(capacity)
    // function to call when adding new object
    private var initializer: ((N, T) -> Unit)? = initializer
    // function to call when removing
    private var destructor: ((N, T) -> Unit)? = destructor

    // push to cache
    // object is pushed to the beginning of the queue
    suspend fun push(label: N, obj: T, callInit: Boolean = true) =
        mutex.withLock {
            val index = queue.indexOfFirst{ it.label == label }
		    var item: QueueItem<N>
            if (index == -1) {
                // label does not exist
                var lastIndex = queue.size
                if (queue.size < capacity) {
                    // cache is not full
                    objects.add(obj)
                } else {
                    // pop least recently used item and replace with the new one
                    lastIndex = queue.removeLast().index
                    objects[lastIndex] = obj
                }
			    item = QueueItem(lastIndex, label)
                queue.addFirst(item)
            } else {
                // label exists. overwrite with new object
                item = queue.removeAt(index)
                queue.addFirst(item)
                objects[item.index] = obj
            }
		    if (callInit) {
			    try{ initializer?.invoke(label, obj) } catch (e: Exception) {}
		    }
        }


    fun pushSync(label: N, obj: T, callInit: Boolean = true) =
        runBlocking{ push(label, obj, callInit) }


    // get from cache
    // object is pushed to the beginning of the queue
    suspend fun get(label: N): T? =
        mutex.withLock {
            val index = queue.indexOfFirst{ it.label == label }
            if (index == -1) {
                return null
            }
            val item = queue.removeAt(index)
            queue.addFirst(item)
            return objects[item.index]
        }

    fun getSync(label: N): T? = runBlocking { get(label) }

    // pop from cache
    suspend fun pop(label: N, callDestruct: Boolean = true): T? =
        mutex.withLock {
            return _pop(label, callDestruct)
        }

    private fun _pop(label: N, callDestruct: Boolean): T? {
        val index = queue.indexOfFirst{ it.label == label }
        if (index == -1) {
            return null
        }
        val item = queue.removeAt(index)
        // fix indexes
        queue.forEach{
            if (it.index >= item.index) {
                it.index--
            }
        }
		val obj = objects.removeAt(item.index)
		if (callDestruct) {
			try{ destructor?.invoke(label, obj) } catch (e: Exception) {}
		}
        return obj
    }

    // check object exists
    suspend fun exists(label: N): Boolean = mutex.withLock {
        queue.indexOfFirst{ it.label == label } != -1
    }

    fun existsSync(label: N): Boolean = runBlocking { exists(label) }

    // clear cache
    suspend fun clear(callDestruct: Boolean = true) =
        mutex.withLock {
		    if (callDestruct) {
			    queue.forEach{
				    try{
                        destructor?.invoke(it.label, objects[it.index])
                    } catch (e: Exception) {}
			    }
		    }
            objects.clear()
            queue.clear()
        }

    fun clearSync(callDestruct: Boolean = true) = runBlocking { clear(callDestruct) }

    // set cache capacity
    suspend fun setCapacity(newc: Int, callDestruct: Boolean = true) {
        if (capacity < 1) {
            throw(IllegalArgumentException("Capacity must be positive"))
        } else {
            mutex.withLock {
                if (capacity < newc) {
                    // increase cache capacity
                    capacity = newc
                } else if (capacity > newc) {
                    // cut cache capacity
                    for (i in (queue.size-1) downTo newc) {
                        val item = queue[i]
                        _pop(item.label, callDestruct)
                    }
                    capacity = newc
                }
            }
        }
    }

    fun setCapacitySync(newc: Int, callDestruct: Boolean = true) =
        runBlocking { setCapacity(newc, callDestruct) }

    // ensure that cache capacity is not smaller then requested
    suspend fun ensureCapacity(newc: Int) {
        if (capacity < 1) {
            throw(IllegalArgumentException("Capacity must be positive"))
        } else {
            mutex.withLock {
                if (capacity < newc) {
                    // increase cache capacity
                    capacity = newc
                }
            }
        }
    }

    fun ensureCapacitySync(newc: Int) = runBlocking { ensureCapacity(newc) }

    suspend fun getCapacity(): Int = mutex.withLock { capacity }

    fun getCapacitySync(): Int = runBlocking { getCapacity() }

    suspend fun getObjects(): ArrayList<T> = mutex.withLock { objects }

    fun getObjectsSync(): ArrayList<T> = runBlocking { getObjects() }

    suspend fun getOrder(): ArrayList<Int> = mutex.withLock {
        ArrayList<Int>(queue.size).apply{
            queue.forEach{
                this.add(it.index)
            }
        }
    }

    fun getOrderSync(): ArrayList<Int> = runBlocking { getOrder() }

    suspend fun getLabels(): ArrayList<N> = mutex.withLock {
        ArrayList<N>(queue.size).apply{
            queue.forEach{
                this.add(it.label)
            }
        }
    }

    fun getLabelsSync(): ArrayList<N> = runBlocking { getLabels() }

    suspend fun getObjectsOrdered(): ArrayList<T> = mutex.withLock {
        ArrayList<T>(queue.size).apply{
            queue.forEach{
                this.add(objects[it.index])
            }
        }
    }

    fun getObjectsOrderedSync(): ArrayList<T> = runBlocking { getObjectsOrdered() }

}
