// package org.gitlab.matsievskiysv.bookreader.pagedview

// import android.graphics.Bitmap
// import android.graphics.Canvas
// import android.graphics.Color
// import android.graphics.Matrix
// import android.graphics.Paint
// import android.graphics.Point
// import android.graphics.PointF
// import android.graphics.Rect
// import android.graphics.RectF

// import android.app.Activity
// import android.content.Context
// import android.content.res.Configuration
// import android.util.AttributeSet
// import android.view.GestureDetector
// import android.view.MotionEvent
// import android.view.ScaleGestureDetector
// import android.view.View
// import android.widget.Toast

// import kotlin.math.*

// import kotlinx.coroutines.GlobalScope
// import kotlinx.coroutines.launch
// import kotlinx.coroutines.async
// import kotlinx.coroutines.runBlocking
// import kotlinx.coroutines.Job
// import kotlinx.coroutines.isActive
// import kotlinx.coroutines.sync.Mutex
// import kotlinx.coroutines.sync.withLock

// import kotlin.lazy

// import kotlin.collections.removeAll

// import org.gitlab.matsievskiysv.bookreader.helpers.Logger
// import org.gitlab.matsievskiysv.bookreader.helpers.clip

// @kotlin.ExperimentalStdlibApi
// abstract class PagedAdapter constructor(context: Context,
//                                         view: View,
//                                         document: SharedAccess<IPagedDocument>) {

//     private val logger = Logger(context, this::class.simpleName!!)
//     private var document = document                       // document handler
//     protected var fillMode: FillMode = FillMode.WIDTH      // fill mode for pages
//         set(mode) {
//             if (mode != field) {
//                 // bitmaps now have wrong size
//                 runBlocking {
//                     async { bitmapCache.clear() }
//                 }
//                 field = mode
//             }
//         }
//     protected val view = view                                    // current view
//     public var currentPage = 0                                   // current page
//         set(page) {
//             field = clip(page, 0, maxPage)
//         }
//     public val maxPage: Int by lazy {
//         var pages = 0
//         document.withBlock { pages = it.pageNum() }
//         pages - 1
//     }  // maximum page number. zero indexed
//     protected var pageBounds: MutableList<PageBounds> = ArrayList() // page info
//     private val toast = Toast.makeText(context, "", Toast.LENGTH_SHORT) // notification toast
//     protected var curPos = PointF(0f, 0f) // normalized position of view on page
//     protected var viewSize = Point(0, 0)  // size of view port
//         set(size) {
//             if (size != field ){
//                 // bitmaps now have wrong size
//                 bitmapCache.clearSync()
//                 field = size
//             }
//         }
//     protected var zoomLevel = 1f                   // view zoom level
//     protected var tiles = Point(1, 1)              // number of tiles
//     public var tileFactor = 0.5f                   // tile multiplication factor
//     protected val requiredTiles: MutableList<TileIndex> = ArrayList() // list of tiles required for render
//     // TODO read default number from settings
//     private var pageCache = PageCache(document, 20)    // cache for loaded pages
//     // TODO read default number from settings
//     public var bitmapScaleFactor = 2f                          // scale bitmap
//     protected var bitmapCache = Cache<TileIndex, SharedAccess<Bitmap>>(20, // cache for bitmaps
// 							        null,
// 							        {_, sb -> runBlocking { sb.withLock { it.recycle() }} } )
//     // TODO read default number from settings
//     private val jobList: SharedAccess<MutableMap<TileIndex, Job>> = SharedAccess(HashMap(20))

//     init {
//         resetDocument()
//     }

//     fun close() {
//         runBlocking {
//             async { adapterClose() }
//             async { bitmapCache.clear() }
//             async { pageCache.clear() }
//         }
//         document.withBlock { it.close() }
//     }

//     // recalculate all document data
//     private fun resetDocument() {
//         if (maxPage < 0) {
//             logger.e("Document does not have any pages")
//             // TODO handle gracefully
//             throw RuntimeException("Document does not have any pages")
//         }
//         pageBounds = ArrayList<PageBounds>(maxPage + 1)         // allocate list
//         logger.v("calculating page bounds")
//         runBlocking {
//             for (i in 0..maxPage) {
//                 // TODO add loading indicator
//                 async {
//                     document.withLock{
//                         val page = it.page(i)
//                         if (page == null) {
//                             logger.e("Cannot load page #${i}")
//                             // TODO handle gracefully
//                             throw RuntimeException("Cannot load page")
//                         } else {
//                             pageBounds.add(PageBounds(i, page.size(), RectF(0f, 0f, 1f, 1f)))
//                             // TODO start async crop calculations
//                             page.close()
//                         }
//                     }
//                 }
//             }
//         }
//         pageBounds.sortBy { it.page }
//         logger.v("page bounds calculated")
//     }

//     private fun loadTiles() {
//         val capacity = ceil(requiredTiles.size * 1.2).toInt()                                  // TODO get from settings
//         runBlocking {
//             async { pageCache.ensureCapacity(capacity) }
//             async { bitmapCache.ensureCapacity(capacity) }
//             jobList.withLock{
//                 it.map {
//                     (tile, job) ->
//                         if (!(tile in requiredTiles)) {
//                             logger.i("canceled rendering of tile $tile")
//                             job.cancel()
//                         }
//                 } // cancel not needed jobs
//                 val toRemove = it.filter { (_, job) -> !job.isActive }
//                 toRemove.forEach {
//                     (tile, job) -> it.remove(tile, job)  // pop finished and canceled jobs
//                 }
//             }
//         }
//         runBlocking {
//             for (t in requiredTiles) {
//                 async {
//                     if (!bitmapCache.exists(t)) {
//                         logger.v("loading tile $t")
//                         val (_, tileSize) = getPageTileBitmapSize(t.page)
//                         val bitmap = Bitmap.createBitmap(tileSize.x, tileSize.y,
//                                                          Bitmap.Config.ARGB_8888)
//                         Canvas(bitmap).apply{
//                             drawColor(Color.GRAY)
//                             drawText("${t.page+1}:${t.tile.x+1}x${t.tile.y+1}",
//                                      tileSize.x.toFloat()/2, tileSize.y.toFloat()/2,
//                                      Paint().apply{
//                                          setColor(Color.BLACK)
//                                          setTextAlign(Paint.Align.CENTER)
//                                          setAntiAlias(true)
//                             })
//                         }
//                         val safeBitmap = SharedAccess(bitmap)
//                         bitmapCache.push(t, safeBitmap)
//                         val job = GlobalScope.launch {
//                             if (isActive) {
//                                 pageCache.withPage (t.page) {
//                                     val page = it
//                                     if (isActive) {
//                                         safeBitmap.withBlock {
//                                             val tile = RectF(pageBounds[t.page].cropBounds.width() *
//                                                              t.tile.x / t.tiles.x +
//                                                              pageBounds[t.page].cropBounds.left,
//                                                              pageBounds[t.page].cropBounds.height() *
//                                                              t.tile.y / t.tiles.y +
//                                                              pageBounds[t.page].cropBounds.top,
//                                                              pageBounds[t.page].cropBounds.width() *
//                                                              (t.tile.x+1) / t.tiles.x +
//                                                              pageBounds[t.page].cropBounds.left,
//                                                              pageBounds[t.page].cropBounds.height() *
//                                                              (t.tile.y+1) / t.tiles.y +
//                                                              pageBounds[t.page].cropBounds.top)
//                                             page.draw(it, tileSize, tile)
//                                         }
//                                     }
//                                 }
//                             }
//                             runBlocking{
//                                 async {
//                                     jobList.withLock {
//                                         it.remove(t)
//                                     }
//                                 }
//                             }
//                             view.postInvalidate()
//                         }
//                         runBlocking{
//                             async {
//                                 jobList.withLock {
//                                     it.put(t, job)
//                                 }
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//     }

//     fun draw(canvas: Canvas, viewSize: Point) {
//         this.viewSize = viewSize
//         updateRequiredTiles(viewSize)
//         loadTiles()
//         composeTiles(canvas)
//     }

//     open protected fun adapterClose() {}
//     abstract protected fun pageToViewScale(page: Int): Float
//     abstract protected fun updateRequiredTiles(size: Point)
//     abstract protected fun offsetPage(page: Int): PointF
//     abstract protected fun composeTiles(canvas: Canvas)
//     abstract public fun fling(velocityX: Float, velocityY: Float): Boolean
//     abstract public fun scroll(distanceX: Float, distanceY: Float): Boolean
//     abstract public fun zoom(detector: ScaleGestureDetector): Boolean

//     // tile index
//     protected data class TileIndex(val page: Int,                                                         // page number
//                                    val tile: Point,                                                        // tile index
//                                    val tiles: Point)                                                  // number of tiles

//     companion object {
//         public enum class FillMode {
//             WIDTH, HEIGHT, PAGE
//         }
//     }

// }
