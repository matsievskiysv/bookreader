package org.gitlab.matsievskiysv.bookreader.codec

import android.graphics.Point
import android.graphics.Bitmap
import android.graphics.RectF

import com.artifex.mupdf.fitz.Page
import com.artifex.mupdf.fitz.Rect
import com.artifex.mupdf.fitz.Matrix
import com.artifex.mupdf.fitz.android.AndroidDrawDevice

import kotlin.math.abs
import kotlin.math.round
import kotlin.math.min

import org.gitlab.matsievskiysv.bookreader.pagedview.IPagedPage

class MuPDFPage constructor(page: Page): IPagedPage {

    private val page = page
    private var pageSize = Point(-1, -1)
    private val matrix = Matrix()


    init {
        val bounds: Rect = page.getBounds()
        pageSize = Point(abs(round(bounds.x1-bounds.x0)).toInt(),
                         abs(round(bounds.y1-bounds.y0)).toInt())
    }

    override fun close() {
        page.destroy()
    }

    override fun size(): Point {
        return pageSize
    }

    override fun draw(bitmap: Bitmap, size: Point, crop: RectF) {
        val ctm = Matrix();
        val scale = size.x.toFloat() / pageSize.x.toFloat()     // it should not matter if we use x of y for calculation
        ctm.scale(scale, scale)                                 // scale whole page to fit
        ctm.scale(1/crop.width(), 1/crop.height())              // scale tile to fit
        ctm.e = -crop.left * pageSize.x * scale / crop.width()  // move slice to the left
        ctm.f = -crop.top * pageSize.y * scale / crop.height()  // move slice to the up
        val dev = AndroidDrawDevice(bitmap, 0, 0, 0, 0, size.x, size.y)
        page.run(dev, ctm, null);
        dev.close();
        dev.destroy();
    }

    private fun convertMatrix(matrix: Matrix): android.graphics.Matrix {
        return android.graphics.Matrix().apply{
            setValues(floatArrayOf(matrix.a, matrix.c, matrix.e,
                                   matrix.b, matrix.d, matrix.f,
                                   0f, 0f, 0f))}
    }
}
