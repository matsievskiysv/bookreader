package org.gitlab.matsievskiysv.bookreader

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.graphics.Rect

import android.content.Context

import android.net.Uri

// import com.davemorrissey.labs.subscaleview.ImageSource
// import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
// import com.davemorrissey.labs.subscaleview.decoder.CompatDecoderFactory
// import com.davemorrissey.labs.subscaleview.decoder.ImageDecoder
// import com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder
// import com.davemorrissey.labs.subscaleview.decoder.SkiaImageDecoder
// import com.davemorrissey.labs.subscaleview.decoder.SkiaImageRegionDecoder
// import com.davemorrissey.labs.subscaleview.test.R.id
// import com.davemorrissey.labs.subscaleview.test.R.layout

import org.gitlab.matsievskiysv.bookreader.helpers.Logger

class MainActivity: AppCompatActivity() {
    private lateinit var logger: Logger
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

	val sharedPref = getSharedPreferences("MAIN", Context.MODE_PRIVATE)
        with (sharedPref.edit()) {
            putInt("LOGLEVEL", Logger.VERBOSE)
            commit()
        }

	logger = Logger(this, "prefix")
	logger.d("setup logger")

        setContentView(R.layout.viewer)
	// var imageView = findViewById(R.id.viewer_image) as SubsamplingScaleImageView
	// imageView.setRegionDecoderClass(MyImageDecoder::class.java)
	// imageView.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CENTER_CROP)
	// imageView.setDebug(true)
	// imageView.setImage(ImageSource.resource(R.drawable.map))
	// val imageView = findViewById(R.id.viewer_image) as ImageView
	// val scale = getResources().getDisplayMetrics().density
	// val height = getResources().getDisplayMetrics().heightPixels
	// val width = getResources().getDisplayMetrics().widthPixels
	// val imageBitmap = Bitmap.createBitmap(width, 10*height, Bitmap.Config.ARGB_8888)
	// val canvas = Canvas(imageBitmap)
	// canvas.drawColor(Color.WHITE)
	// val p = Paint()
	// p.setColor(Color.BLUE)
	// p.setTextSize(24*scale)
	// canvas.drawText("Hello", width.toFloat()/2, height.toFloat()/2, p)
	// imageView.setImageBitmap(imageBitmap)
    }
}

// public class MyImageDecoder: ImageRegionDecoder {

//     private var ready = true
//     private var col: Array<Int> = arrayOf(Color.RED,
// 					  Color.GREEN,
// 					  Color.BLUE)
//     private var coli: Int = 0

//     override fun init(context: Context, uri: Uri): Point {
// 	return Point(5000, 500000)
//     }

//     override fun decodeRegion(sRect: Rect, sampleSize: Int): Bitmap {
// 	android.util.Log.e("EEEEE", "render")
// 	val bitmap = Bitmap.createBitmap(sRect.width() / sampleSize,
// 					 sRect.height() / sampleSize,
// 					 Bitmap.Config.ARGB_8888)
// 	bitmap.eraseColor(col[coli])
// 	coli++
// 	if (coli == col.size) {
// 	    coli = 0
// 	}
// 	return bitmap
//     }

//     override fun isReady(): Boolean {
// 	return ready
//     }

//     override fun recycle() {
// 	ready = false
//     }
// }
