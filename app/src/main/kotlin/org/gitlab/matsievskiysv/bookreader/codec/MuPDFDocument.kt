package org.gitlab.matsievskiysv.bookreader.codec

import android.net.Uri
import android.graphics.Canvas
import android.graphics.Point

import com.artifex.mupdf.fitz.Document
import com.artifex.mupdf.fitz.Page

import org.gitlab.matsievskiysv.bookreader.pagedview.IPagedDocument
import org.gitlab.matsievskiysv.bookreader.pagedview.IPagedPage

class MuPDFDocument constructor(path: String): IPagedDocument {

    private val document = Document.openDocument(path)                        // TODO use accelerator

    override fun close() {
        document.destroy()
    }

    override fun needsPassword(): Boolean {
        return document.needsPassword()
    }

    override fun authenticate(password: String): Boolean {
        return document.authenticatePassword(password)
    }

    override fun pageNum(): Int {
	    return document.countPages()
    }

    override fun page(page: Int): IPagedPage? {
        try {
            return MuPDFPage(document.loadPage(page))
        } catch (e: Exception) {
            return null
        }
    }
}
