package org.gitlab.matsievskiysv.bookreader.codec

import android.net.Uri
import android.content.Context
import android.content.ContentResolver
import android.database.Cursor
import androidx.core.content.ContextCompat

import org.gitlab.matsievskiysv.bookreader.helpers.RealPathUtil
import org.gitlab.matsievskiysv.bookreader.pagedview.IPagedDocument
import org.gitlab.matsievskiysv.bookreader.pagedview.SharedAccess

class Codec {
    companion object {
        fun getDoc(context: Context, uri: Uri): SharedAccess<IPagedDocument>? {
            when (context.getContentResolver().getType(uri)) {
                "application/pdf" -> {
                    val doc = MuPDFDocument(RealPathUtil.getRealPath(context, uri)!!)
                    if (doc == null) {
                        return null
                    } else {
                        return SharedAccess(doc)
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}
