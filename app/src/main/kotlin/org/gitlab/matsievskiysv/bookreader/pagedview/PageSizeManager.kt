package org.gitlab.matsievskiysv.bookreader.pagedview

import android.graphics.Point
import android.graphics.PointF
import android.graphics.Rect
import android.graphics.RectF

public class PageSizeManager(pageNum: Int, scaleFunction: (Point, Point) -> Float) {
    private val origPages = List(pageNum) {
        OrigBounds(Point(0, 0),
                   RectF(0f, 0f, 0f, 0f),
                   Rect(0, 0, 0, 0))
    }
    private val scaledPages = List(pageNum) {
        ScaledBounds(Point(0, 0),
                     Point(0, 0),
                     1f,
                     Point(1, 1))
    }
    public var scale = 1f
    public var viewSize = Point(0, 0)
    public var padding = Rect(0, 0, 0, 0)
    public var scaleFunction = scaleFunction

    public var crop = false

    fun setOrigSize(page: Int, size: Point) {
        origPages[page].origSize = size
    }

    fun getOrigSize(page: Int): Point = origPages[page].origSize

    // protected fun getPageTileSize(page: Int): Pair<Point, Point> {
    //     val (_, tileBitmapSize) = getPageTileBitmapSize(page)
    //     val tileSize: Point = (tileBitmapSize * zoomLevel / tiles / bitmapScaleFactor).ceil()
    //     val pageSize: Point = tileSize * tiles
    //     return Pair(pageSize, tileSize)
    // }

    // private fun getPageTileBitmapSize(page: Int): Pair<Point, Point> {
    //     val scale = pageToViewScale(currentPage)
    //     val pageSizeFit: PointF = pageBounds[currentPage].pageSize / scale *
    //     bitmapScaleFactor * PointF(pageBounds[page].cropBounds.width(),
    //                                pageBounds[page].cropBounds.height())
    //     tiles = max((pageSizeFit * zoomLevel / viewSize * tileFactor).floor(), Point(1, 1))
    //     val tileSize: Point = pageSizeFit.ceil()
    //     val pageSize: Point = tileSize * tiles
    //     return Pair(pageSize, tileSize)
    // }

    fun getPageBitmapSize(page: Int, size: Point): Point = origPages[page].origSize


    private data class OrigBounds(var origSize: Point,
                                  var crop: RectF,
                                  var coord: Rect)

    private data class ScaledBounds(var pageSize: Point,
                                    var tileSize: Point,
                                    var scale: Float,
                                    var tiles: Point)
}
