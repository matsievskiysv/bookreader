package org.gitlab.matsievskiysv.bookreader.viewer

import org.gitlab.matsievskiysv.bookreader.R

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

// import android.content.Context;
import android.view.View
import android.widget.Button
// import android.widget.Toast;

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction

import androidx.constraintlayout.widget.ConstraintLayout
import android.view.ViewGroup
import android.content.Context
import androidx.core.content.PermissionChecker
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import android.Manifest
import android.net.Uri

import org.gitlab.matsievskiysv.bookreader.helpers.Logger
import org.gitlab.matsievskiysv.bookreader.codec.Codec
import org.gitlab.matsievskiysv.bookreader.pagedview.PagedView

@kotlin.ExperimentalStdlibApi
class ViewerActivity: AppCompatActivity() {
    private lateinit var logger: Logger
    private lateinit var fileUri: Uri

    val requestPermissionLauncher =
        registerForActivityResult(RequestPermission()) {
            isGranted: Boolean ->
                if (isGranted) {
                    showPager()
                } else {
                    finish()
                }
        }

    private fun showPager() {
        val doc = Codec.getDoc(this, fileUri)
        if (doc != null) {
            val container = findViewById<ViewGroup>(R.id.viewer_container)
            val pageView = PagedView(this, null)
            pageView.setDocument(doc)
            container.addView(pageView, 0,
                              ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                     ViewGroup.LayoutParams.MATCH_PARENT))
        } else {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.viewer)
        setSupportActionBar(findViewById(R.id.viewer_toolbar))

        val sharedPref = getSharedPreferences("MAIN", Context.MODE_PRIVATE)
        with (sharedPref.edit()) {
            putInt("LOGLEVEL", Logger.VERBOSE)
            commit()
        }

        logger = Logger(this, "ViewerActivity")
        logger.v("onCreate")

        fileUri = getIntent().getData()!!
        if (PermissionChecker
            .checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE")
            != PermissionChecker.PERMISSION_GRANTED) {
            logger.w("permissions required")
            requestPermissionLauncher.launch(
                Manifest.permission.READ_EXTERNAL_STORAGE)
        } else {
            showPager()
        }
    }
}
