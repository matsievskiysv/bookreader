// package org.gitlab.matsievskiysv.bookreader.pagedview

// import android.graphics.Bitmap
// import android.graphics.Canvas
// import android.graphics.Point
// import android.graphics.PointF
// import android.graphics.Rect
// import android.graphics.RectF
// import android.graphics.Matrix
// import android.graphics.Paint

// import android.view.ScaleGestureDetector
// import android.animation.ObjectAnimator
// import android.view.animation.LinearInterpolator
// import android.content.Context
// import android.view.View


// import kotlin.math.*

// import kotlinx.coroutines.async
// import kotlinx.coroutines.runBlocking

// import org.gitlab.matsievskiysv.bookreader.helpers.Logger

// @kotlin.ExperimentalStdlibApi
// class VerticalScrollAdapter constructor(context: Context,
//                                         view: View,
//                                         document: SharedAccess<IPagedDocument>):
//     PagedAdapter(context, view, document) {

//     private val logger = Logger(context, this::class.simpleName!!)

//     override fun pageToViewScale(page: Int): Float {
//         return when (fillMode) {
//             PagedAdapter.Companion.FillMode.WIDTH -> {
//                 pageBounds[page].pageSize.x *
//                 pageBounds[page].cropBounds.width() / viewSize.x
//             }
//             PagedAdapter.Companion.FillMode.HEIGHT -> {
//                 logger.e("FillMode.HEIGHT is not available in vertical scroll mode")
//                 throw RuntimeException("FillMode.HEIGHT is not available in vertical scroll mode")
//             }
//             PagedAdapter.Companion.FillMode.PAGE -> {
//                 val scaleX = pageBounds[page].pageSize.x *
//                 pageBounds[page].cropBounds.width() / viewSize.x
//                 val scaleY = pageBounds[page].pageSize.y *
//                 pageBounds[page].cropBounds.height() / viewSize.y
//                 if (abs(scaleX-1) < abs(scaleY-1)) scaleX else scaleY
//             }
//         }
//     }

//     override fun updateRequiredTiles(size: Point) {
//         var (pageSize, tileSize) = getPageTileSize(currentPage)

//         requiredTiles.clear()
//         getStartEndPoints(currentPage, pageSize * curPos, PointF(size)).forEach{
//             val (page, view) = it
//             requiredTiles += getTiles(page, tileSize, view.roundOut())
//         }
//     }

//     private fun getStartEndPoints(page: Int, offset: PointF, size: PointF): List<Pair<Int, RectF>> {
//         fun clipH(pos: PointF, pageSize: PointF): Pair<Float, Float> {
//             return Pair(min(pageSize.x - viewSize.x, pos.x),
//                         min(pageSize.x, pos.x + viewSize.x))
//         }

//         fun findBottom(startPage: Int,
//                        initialOffset: Float,
//                        distance: Float): Triple<Int, Float, Float> {
//             var dy = distance
//             var point = initialOffset                          // starting point
//             @Suppress("NAME_SHADOWING") var page = startPage
//             while (true) {
//                 val (pageSize, _) = getPageTileSize(page)
//                 val delta = min(pageSize.y - point, dy)
//                 dy -= delta
//                 point += delta
//                 if (page == maxPage || dy <= 0) {
//                     break
//                 }
//                 page++
//                 point = 0f
//             }
//             dy = if (dy > 0) abs(dy) else 0f
//             return Triple(page, point, dy)
//         }

//         fun findTop(startPage: Int,
//                     initialOffset: Float,
//                     distance: Float): Triple<Int, Float, Float> {
//             var dy = distance
//             @Suppress("NAME_SHADOWING") var page = startPage
//             var point = initialOffset / getPageTileSize(page).first.y // normalized end point
//             while (true) {
//                 val (pageSize, _) = getPageTileSize(page)
//                 val delta = min(point * pageSize.y, dy)
//                 dy -= delta
//                 point -= delta / pageSize.y
//                 if (page == 0 || dy <= 0) {
//                     break
//                 }
//                 page--
//                 point = 1f
//             }
//             dy = if (dy > 0) abs(dy) else 0f
//             return Triple(page, point * getPageTileSize(page).first.y, dy)
//         }

//         val firstPage: Int
//         val topPoint: Float
//         val topOverflow: Float
//         val lastPage: Int
//         val bottomPoint:Float
//         val bottomOverflow:Float

//         if (size.y > 0) {
//             // scroll forward
//             findBottom(page,
//                        offset.y,
//                        abs(size.y)).let { lastPage = it.first
//                                           bottomPoint = it.second
//                                           bottomOverflow = it.third }
//             findTop(page,
//                     offset.y,
//                     bottomOverflow).let { firstPage = it.first
//                                           topPoint = it.second
//                                           topOverflow = it.third }
//             // TODO use topOverflow in padding calculations
//         } else {
//             // scroll backward
//             findTop(page,
//                     offset.y,
//                     abs(size.y)).let { firstPage = it.first
//                                        topPoint = it.second
//                                        topOverflow = it.third }
//             findBottom(page,
//                        offset.y,
//                        topOverflow).let { lastPage = it.first
//                                           bottomPoint = it.second
//                                           bottomOverflow = it.third }
//             // TODO use bottomOverflow in padding calculations
//         }
//         val pages = (firstPage..lastPage).map {
//             val (pageSize, _) = getPageTileSize(it)
//             val (leftPoint, rightPoint) = clipH(offset, PointF(pageSize))
//             Pair(it, RectF(leftPoint, 0f, rightPoint, pageSize.y.toFloat()))
//         }
//         pages[0].second.top = topPoint                      // first page offset
//         pages[pages.size - 1].second.bottom = bottomPoint   // last page offset
//         return pages
//     }

//     private fun getTiles(page: Int, tileSize: Point, view: Rect): List<TileIndex> {
//         val leftIndex = floor(view.left.toFloat() / tileSize.x).toInt()
//         val topIndex = floor(view.top.toFloat() / tileSize.y).toInt()
//         val rightIndex = ceil(view.right.toFloat() / tileSize.x).toInt()
//         val bottomIndex = ceil(view.bottom.toFloat() / tileSize.y).toInt()

//         val tileList = ArrayList<TileIndex>((rightIndex-leftIndex) * (bottomIndex-topIndex))

//         for (i in leftIndex..(rightIndex-1)) {
//             for (j in topIndex..(bottomIndex-1)) {
//                 tileList.add(TileIndex(page, Point(i, j), tiles))
//             }
//         }
//         return tileList
//     }

//     override fun offsetPage(page: Int): PointF {
//         val (pageSize, _) = getPageTileSize(page)
//         return when (fillMode) {
//             PagedAdapter.Companion.FillMode.WIDTH ->
//                 PointF(0f, if (viewSize.y > pageSize.y) (viewSize.y - pageSize.y).toFloat() / 2 else 0f)
//             PagedAdapter.Companion.FillMode.HEIGHT -> {
//                 logger.e("FillMode.HEIGHT is not available in vertical scroll mode")
//                 throw RuntimeException("FillMode.HEIGHT is not available in vertical scroll mode")
//             }
//             PagedAdapter.Companion.FillMode.PAGE -> if ((viewSize.x - pageSize.x) >=
//                                                              (viewSize.y - pageSize.y)) {
//                 PointF(if (viewSize.x > pageSize.x) (viewSize.x - pageSize.x).toFloat() / 2 else 0f, 0f)
//             } else {
//                 PointF(0f, if (viewSize.y > pageSize.y) (viewSize.y - pageSize.y).toFloat() / 2 else 0f)
//             }
//         }
//     }

//     override fun composeTiles(canvas: Canvas) {
//         // ObjectAnimator.ofFloat(view, "alpha", 0f).apply {
//         //     duration = 20
//         //     start()
//         // }
//         val usedPages = requiredTiles.map { it.page }.distinct().sortedBy { it }
//         val minPage = usedPages.minBy { it } ?: 0
//         val pageOffsets = ArrayList<Int>(usedPages.size).apply {
//             var accum = 0
//             usedPages.forEach{
//                 this.add(accum)
//                 getPageTileBitmapSize(it).run { accum += first.y }
//             }
//         }
//         for (t in requiredTiles) {
//             val (_, tileSize) = getPageTileBitmapSize(currentPage)
//             val (pageSize, _) = getPageTileSize(currentPage)
//             val tileSizeF = PointF(tileSize)
//             val pos = pageSize * curPos
//             val offset = offsetPage(t.page)

//             bitmapCache.getSync(t)?.withBlock {
//                 val matrix = Matrix().apply{
//                     postTranslate(tileSizeF.x * t.tile.x,
//                                   tileSizeF.y * t.tile.y)
//                     postTranslate(0f,
//                                   pageOffsets[t.page - minPage].toFloat())
//                     postScale(zoomLevel / tiles.x / bitmapScaleFactor,
//                               zoomLevel / tiles.y / bitmapScaleFactor)
//                     // postTranslate(offset.x, offset.y)
//                     postTranslate(-pos.x, -pos.y)
//                 }
//                 canvas.drawBitmap(it, matrix, null)
//             } ?: view.postInvalidate()
//         }
//         // ObjectAnimator.ofFloat(view, "alpha", 1f).apply {
//         //     setStartDelay(20)
//         //     duration = 20
//         //     start()
//         // }
//     }

//     override fun fling(velocityX: Float, velocityY: Float): Boolean {
//         return false
//     }

//     override fun scroll(distanceX: Float, distanceY: Float): Boolean {
//         val (pageSize, _) = getPageTileSize(currentPage)
//         val (page, newPos) = getStartEndPoints(currentPage,
//                                                pageSize * curPos,
//                                                PointF(distanceX, distanceY)).let {
//             if (distanceY >= 0) {
//                 // scroll forward
//                 val (page, rect) = it[it.size - 1]
//                 Pair(page, rect.bottomLeft() / getPageTileSize(page).first)
//             } else {
//                 // scroll backward
//                 val (page, rect) = it[0]
//                 Pair(page, rect.topLeft() / getPageTileSize(page).first)
//             }
//         }

//         if (curPos != newPos || currentPage != page) {
//             curPos = newPos
//             currentPage = page
//             view.postInvalidate()
//             return true
//         }
//         return false
//     }

//     override fun zoom(detector: ScaleGestureDetector): Boolean {
//         val (pageSize, _) = getPageTileSize(currentPage)
//         // translate to the focus point with vector length proportional to zoom level
//         val focus = curPos * pageSize + PointF(detector.getFocusX(), detector.getFocusY())
//         // val viewCenter = PointF(curPos.x * pageSize.x + viewSize.x.toFloat() / 2,
//         //                         curPos.y * pageSize.y + viewSize.y.toFloat() / 2)
//         val translateVector = (focus - curPos * pageSize) * (detector.getScaleFactor() - 1)
//         val pos = curPos * pageSize + translateVector
//         val newPos = pos.clip(PointF(0f, 0f), PointF(pageSize - viewSize).apply { y = Float.POSITIVE_INFINITY } ) / pageSize
//         val zoom = clip(zoomLevel * detector.getScaleFactor(), 1f, 100f)
//         if (zoom != zoomLevel || newPos != curPos) {
//             zoomLevel = zoom
//             curPos = newPos
//             view.postInvalidate()
//             return true
//         }
//         return false
//     }

// }
