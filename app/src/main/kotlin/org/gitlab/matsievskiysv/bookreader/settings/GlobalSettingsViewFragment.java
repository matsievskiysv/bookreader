package org.gitlab.matsievskiysv.bookreader.settings;

import org.gitlab.matsievskiysv.bookreader.R;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.app.Application;
import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.content.Context;
import android.widget.Button;
import android.content.Intent;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.net.Uri;
import android.provider.DocumentsContract;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GlobalSettingsViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GlobalSettingsViewFragment extends Fragment {

    private static final int CHOOSE_DIR_ACTIVITY = 100;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public GlobalSettingsViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GlobalSettingsViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GlobalSettingsViewFragment newInstance(String param1, String param2) {
        GlobalSettingsViewFragment fragment = new GlobalSettingsViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.global_settings_view, container, false);
        final Button button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    // intent.setType("inode/directory");
                    startActivityForResult(intent, CHOOSE_DIR_ACTIVITY);
                }
            });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_DIR_ACTIVITY) {
            Context context = getActivity().getApplication().getApplicationContext();
            if(resultCode == Activity.RESULT_OK){
                Uri uri=data.getData();
                Toast.makeText(context, uri.toString(), Toast.LENGTH_LONG).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
