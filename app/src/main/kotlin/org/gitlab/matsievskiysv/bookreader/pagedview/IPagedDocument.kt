package org.gitlab.matsievskiysv.bookreader.pagedview

import android.graphics.Canvas
import android.graphics.Point

interface IPagedDocument {
    // close document
    fun close() {}
    // document is password protected
    fun needsPassword(): Boolean { return false }
    // authenticate
    fun authenticate(password: String): Boolean { return true }
    // get number of pages
    fun pageNum(): Int {return -1}
    // get page
    fun page(page: Int): IPagedPage?
}
